/* globals moment, d3 */

const INVERTED = true;

moment.locale("it");


const colorScale = d3.scaleOrdinal(d3.schemeSet3);

let margin = {top:100,left:100,right:100,bottom:20};
const height = d3.select("#container").node().clientHeight - 10;
const width = window.innerWidth;
const w = width - margin.left - margin.right;
const h = height - margin.bottom - margin.top;
const svg = d3.select("#container").append("svg").attr("width", width).attr("height", height).style("cursor", "move");




// DOM
d3.select("html").style("color", INVERTED ? "#EBEBEB" : "#333333").style("background", INVERTED ? "#000" : "#FFF");
const tooltip = d3.select("body").append("div").attr("id", "tooltip");




let arrivi; 
let partenze;
let $partenze;
let $arrivi;
let state = {isArrival: true};


svg.on("mousemove", () =>{
	tooltip.style("transform", `translate(${d3.event.x +20}px, ${d3.event.y+20}px)`);
});

d3.csv("arrivi.csv", (error, data)=>{
	if(error) throw new Error(error);
	arrivi = nestData(data, true);
	d3.csv("partenze.csv", (error, data)=>{
		
		if(error) throw new Error(error);
		partenze = nestData(data, false);
		
		$partenze = createGraph(svg.append("g"), partenze, false);
		$arrivi = createGraph(svg.append("g"), arrivi, true);
		d3.select("#buttons")
		.selectAll("button")
		.data(["partenze","arrivi"])
		.enter()
		.append("button")
		.text(d=>d)
		.on("click", ()=>{
			state.isArrival=!state.isArrival;
			updateView();
		});
		updateView();
	});
});

function updateView() {
	d3.select("#buttons").selectAll("button").style("opacity", d=> d=="partenze" ? (state.isArrival ? .5 : 1) : (state.isArrival ? 1 : .5));
	d3.select("header h3").text(state.isArrival ? "Orari voli stagionali in arrivo a Bari Palese e partenza navette per Matera" : "Orari voli stagionali in partenza da Bari Palese e arrivo navette da Matera");
	$arrivi.style("opacity", state.isArrival ? 1 : 0).style("pointer-events",  state.isArrival ? "auto" : "none" );
	$partenze.style("opacity", !state.isArrival ?  1 : 0).style("pointer-events",  !state.isArrival ? "auto" : "none" );
}

function createGraph(chartContainer, nestedData, isArrival) {

	// SCALES
	const band = d3.scaleLinear().domain([0,nestedData.length-1]).range([0,h]);

	chartContainer
	.append("g")
	.attr("transform", (d,i)=>`translate(${margin.left},${margin.top})`)
	.selectAll("g")
	.data(nestedData)
	.enter()
	.append("g")
	.attr("class", "container")
	.attr("transform", (d,i)=>`translate(0, ${band(i)})`)
	.each(function(d,i){
		addShapes.call(this, d,i);
		update.call(this, d);
	});



	chartContainer
	.insert("rect", ":first-child")
	.attr("width", w)
	.attr("height", h+margin.top)
	.style("fill", "rgba(255,0,0,0)")
	.attr("transform", (d,i)=>`translate(${margin.left},0)`)
	.call(d3.zoom().on("zoom", zoomed));

	d3.selectAll("text").attr("fill", INVERTED ? "#AAAAAA" : "#333");
	
	

	function addShapes(d,i) {

		let container = d3.select(this);

		let busTimes = getBusTimes(d.date, isArrival);
		busTimes = busTimes.map(d=>{
			if(d.getHours() == 0){
				d = d3.timeDay.offset(d,1);
			}
			return d;
		});

		let alldayflights = [];
		d.values.forEach(dd=>dd.values.forEach(ddd=>alldayflights.push(ddd)));

		container.append("path")
		.attr("class", "areaPath")
		.style("opacity", .5)
		.attr("fill", "#206979");

		container
		.selectAll(".icon")
		.data(alldayflights)
		.enter()
		.append("g")
		.html(isArrival ? FLIGHTICON_ARRIVAL : FLIGHTICON_DEPARTURE)
		.attr("fill", d=> INVERTED ? colorScale(d.origin): "#333")

		.attr("class", "icon")
		.style("cursor", "pointer")
		.style("opacity", .8)
		.on("mouseout", function(){
			// d3.select(this).attr("stroke", "none");
			updateTooltip(null);
		})
		.on("mouseover", function(d){
			// d3.select(this).attr("stroke", "black");
			updateTooltip(getSingleFlightTooltip(d));
		});

		
		container.append("text").attr("class", "day").text(moment(d.date).format("dddd")).attr("text-anchor", "end").attr("transform", "translate(-20,0)");
		container.append("line").attr("class", "htick").attr("x1", 0).attr("y1", 0).attr("x2", w).attr("y2", 0);



		let busConts = container
		.selectAll(".busCont")
		.data(busTimes)
		.enter()
		.append("g")
		.attr("class", "busCont")


		busConts
		.append("g")
		.html(BUSICON)
		.attr("transform", `translate(0, -40)`)
		.style("cursor", "pointer")
		.attr("fill", "red")
		.on("mouseout", function(d){
			// d3.select(this).style("opacity", .6);
			updateTooltip(null)
		})
		.on("mouseover", function(d){
			// d3.select(this).style("opacity", 1);
			updateTooltip(getBusTooltip(d));
		});

		if(i ==0) {
			
			container
			.insert("g", ":first-child")
			.attr("class", "grid")
			.attr("transform", `translate(0,-${band(1)})`);

		}
	}


	function update(d,t) {

		let container = d3.select(this);
		let startDate = d3.timeDay.floor(d.date);
		// start from 6 in the morning
		startDate = d3.timeHour.offset(startDate,6);
		// end at 2 the next morning
		let endDate = d3.timeHour.offset(startDate,20);
		let timeScale = d3.scaleTime().domain([startDate, endDate]).range([0, w]);
		
		if(t) timeScale.domain(t.rescaleX(timeScale).domain());

		let yScale = d3.scaleLinear().domain([0,20]).range([0,band(1)]);
		var area = d3.area().x(d=>timeScale(d.hour)).y1(d=>-yScale(d.flights)).curve(d3.curveCatmullRom);

		container
		.select(".areaPath")
		.datum(Object.keys(d.flightPerHour).map(key=>d.flightPerHour[key]))
		.style("pointer-events", "none")
		.attr("d", area);



		container
		.selectAll(".icon")
		.style("visibility", d=>{
			let n = timeScale(d.timedate);
			return n<0 || n > w ? "hidden" : "visible";
		})
		.attr("transform", (d,i)=>`translate(${timeScale(d.timedate)},${-10*d.index-20})`);
		
		container
		.selectAll(".busCont")
		.style("visibility",d=>{
			let n = timeScale(d);
			return n<0 || n > w ? "hidden" : "visible";
		})
		.attr("transform", d=>`translate(${timeScale(d)},0)`);

		container
		.select(".grid")
		.call(d3.axisTop(timeScale).ticks(d3.timeHour.every(1)).tickSize(-h-band(1)))



		d3.selectAll(".tick line").style("stroke", "#333").style("opacity", INVERTED ? 1 : .2);
	
	
	}


	function updateTooltip(d) {
		if(!d) {
			tooltip.html('');
			return;
		}
		tooltip.html(d);
	}

	function getSingleFlightTooltip(d) {
		return `<h2>Volo ${isArrival ? "in arrivo da" : "in partenza per"}  ${d.origin} – ${moment(d.timedate).format("HH:mm")}</h2>`;
	}


	function getBusTooltip(d) {
		return `<h2>Autobus ${isArrival ? "in partenza" : "in arrivo"}  – ${moment(d).format("HH:mm")}</h2>`
	}

	function zoomed() {
		d3.selectAll(".container").each(function(d,i){update.call(this, d, d3.event.transform);});
	}

	return chartContainer;


}

function nestData(data, isArrival) {

	let key = isArrival ? "arrival" : "departure"; 

	// set the right time format 
	data.forEach( d=> {

		var hours = d[key].split(":")[0];
		var minutes = d[key].split(":")[1];
		let date = new Date(`${d.date}T00:00Z`);
		date.setUTCHours(hours,minutes);
		d.day = date.getDate();
		// floor hours
		d.hour = d3.timeHour.floor(date).getHours();
		d.timedate = date;

	});

	data.sort((a,b)=>a.timedate-b.timedate);

	// nest by day, hour and city+key to avoid flights in sharing
	let nestedData = d3.nest().key(d=>d.day).key(d=>d.hour).key(d=>d.origin).key(d=>d[key]).entries(data);
	nestedData.forEach( day => {
		
		let date = new Date("2018-03-01T00:00:00");
		date.setDate(+day.key);
		day.date = date;
		day.totalFlights = 0;
		day.flightPerHour = d3.range(0,25).map(d=>{
			let hourTime = new Date(date);
			hourTime.setHours(d);
			return{
				hour:hourTime,
				flights:0
			};
		});
		let flights = [];

		day.values.forEach((hour,index) => {


			hour.values.forEach(origin=>{

				origin.values.forEach(flight=>{
					// flattendown carriers
					if(flight.values.length > 1) {
						flight.values[0].carrier = flight.values.map(d=>d.carrier).join(", ");
						flight.values = [flight.values[0]];
					}	
					let h =  d3.timeHour.ceil(flight.values[0].timedate).getHours();
					day.flightPerHour[h].flights+=1;
					flights.push(flight);
				});

			});
			
			day.flightPerHour[24].flights =0;


			
		});
		day.values = flights;

	});

	//make sure to set different indices to flights that arrive at the same hour
	nestedData.forEach(day=>{
		let previousKey;
		let previousIndex= 0;
		day.values.forEach(flight=>{
			if(previousKey && previousKey === flight.key) {
				previousIndex++;
				flight.values[0].index = previousIndex;
			} else {
				flight.values[0].index = 0;
				previousIndex = 0;
			}
			previousKey = flight.key;
		});
	});

	return nestedData;
	
}

function getBusTimes(date, isArrival){
	
	// partenze bus da bari
	const busDepartureHours = ["09:15", "14:40", "15:45", "19:15", "00:30"];
	// arrivi bus a bari 
	const busArrivalHours = ["06:10", "12:00", "14:15", "18:25", "21:50"];

	// partenze bus se stiamo esaminando arrivi e viceversa
	let hours = isArrival ? busDepartureHours : busArrivalHours;

	let times = [];
	let day = date.getDate();
	let month = date.getMonth();
	let year = date.getFullYear();
	hours.forEach(time=> {
		let d = new Date();
		d.setDate(day);
		d.setMonth(month);
		d.setFullYear(year);
		d.setHours(time.split(":")[0]);
		d.setMinutes(time.split(":")[1]);
		d.setSeconds(0);
		times.push(d);
	});
	return times;
}


const BUSICON = `


<g>
<path d="M13.3,1.4c-0.2-0.1-0.5-0.1-0.6,0V1c0-0.5-0.5-1-1-1H2.2c-0.5,0-1,0.5-1,1v0.5C1,1.4,0.8,1.4,0.6,1.4C0.2,1.6,0,2,0,2.4
v3.4C0,6,0.1,6.1,0.3,6.1S0.6,6,0.6,5.8V2.4C0.6,2.2,0.7,2,0.8,2C1,1.9,1.1,2.1,1.2,2.2v8.4c0,0.5,0.4,1,0.9,1v1.1
c0,0.6,0.5,1.1,1.1,1.1s1.1-0.5,1.1-1.1v-1.1h5.3v1.1c0,0.6,0.5,1.1,1.1,1.1s1.1-0.5,1.1-1.1v-1.1c0.5,0,0.9-0.5,0.9-1V2.2
C12.7,2.1,12.8,1.9,13,2c0.1,0.1,0.2,0.2,0.2,0.4v3.4c0,0.2,0.1,0.3,0.3,0.3c0.2,0,0.3-0.2,0.3-0.3V2.4C13.8,2,13.6,1.6,13.3,1.4z
M9.7,9.6c0-0.4,0.3-0.7,0.7-0.7c0.4,0,0.7,0.3,0.7,0.7c0,0.4-0.3,0.7-0.7,0.7C10,10.3,9.7,10,9.7,9.6z M2.3,2h9.1v4.4H2.3V2z
M3.4,10.3c-0.4,0-0.7-0.3-0.7-0.7c0-0.4,0.3-0.7,0.7-0.7c0.4,0,0.7,0.3,0.7,0.7C4.1,10,3.8,10.3,3.4,10.3z M8.7,9.9H5.2V8.1h3.5
C8.7,8.1,8.7,9.9,8.7,9.9z"/>
</g>

`;

const FLIGHTICON_DEPARTURE = `
<path d="M1.1,5.5c0.1-0.1,2.8,0.7,2.8,0.7C7.4,4.6,6.7,5,8.1,4.3C5.8,2.6,5.8,2.7,5.8,2.5c0-0.1,0.1-0.1,0.1-0.1
c1.5-0.3,0.7-0.3,5,0.4c2.3-1.2,4.8-2.4,4.8-2.4c0.9-0.4,3.3-0.7,3.7,0l0,0C19.8,1.3,18,2.8,17,3.3l-4.2,1.9c-1.9,5.1-2,6.1-2.4,6.4
c-0.9,0.6-1,0.8-1.1,0.7c-0.1-0.1,0.2-1.1,0-5.5c-1,0.4-2.5,1.2-4.9,1.7c-0.6,0.1-1.2,0-1.6-0.3L0.1,6.1C0,6,0,5.9,0.1,5.8L1.1,5.5z
"/>
`;


const FLIGHTICON_ARRIVAL = `
<path d="M1.3,0.6c0.2,0,1.6,2.8,1.6,2.8C7,5.1,6.1,4.7,7.8,5.3C7.4,2.1,7.3,2.1,7.5,2.1c0.1,0,0.1,0,0.2,0c1.5,1,0.8,0.3,3.6,4.4
c2.8,1,5.8,2,5.8,2c1.1,0.4,3.2,2.1,2.9,3l0,0c-0.4,0.9-3.1,0.7-4.2,0.3l-4.9-1.9c-5.7,2.5-6.6,3.2-7.1,3.1
c-1.2-0.2-1.4-0.2-1.4-0.3c-0.1-0.2,1-0.7,4.5-4.4c-1.1-0.4-3-1.1-5.3-2.6c-0.6-0.4-1-0.9-1.1-1.5L0,0.2C0,0.1,0.1,0,0.2,0L1.3,0.6z
"/>
`;