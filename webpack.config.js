const webpack = require('webpack');
const path = require('path');


const UglifyJSPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
	entry: './src/main.js',

	output: {
		filename: 'main.bundle.js',
		path: path.resolve(__dirname, 'dist')
	},
	devtool:"source-map",
	devServer: {
		contentBase: './dist',
		hot: true
	},
	module: {
		rules: [
		{
			test: /\.js$/,
			exclude: /node_modules/,
			loader: 'babel-loader',

			options: {
				presets: ['env']
			}
		},
		{
			test: /\.(scss|css)$/,

			use: [
			{
				loader: 'style-loader'
			},
			{
				loader: 'css-loader'
			},
			{
				loader: 'sass-loader'
			}
			]
		}
		]
	},

	// plugins: [new UglifyJSPlugin()]
};
