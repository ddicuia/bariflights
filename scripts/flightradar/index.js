 var fs = require("fs");
 var moment = require("moment");
 var contents = fs.readFileSync("airport1.json");
 // Define to JSON type
 var jsonContent = JSON.parse(contents);

 var departures = jsonContent.result.response.airport.pluginData.schedule.departures.data;
 departures.forEach(get)

 function get(f) {
 	return console.log(moment(new Date(f.flight.time.scheduled.departure)).format("HH:mm"));	
 }

 /*
 { identification: 
 	{ id: null,
 		row: 4743431687,
 		number: { default: 'W62694', alternative: null },
 		callsign: null,
 		codeshare: null },
 		status: 
 		{ live: false,
 			text: 'Scheduled',
 			icon: null,
 			estimated: null,
 			ambiguous: false,
 			generic: { status: [Object], eventTime: [Object] } },
 			aircraft: 
 			{ model: { code: '320', text: '' },
 			registration: '',
 			images: null },
 			owner: null,
 			airline: 
 			{ name: 'Wizz Air',
 			short: 'Wizz Air',
 			code: { iata: 'W6', icao: 'WZZ' } },
 			airport: 
 			{ origin: { timezone: [Object], info: [Object] },
 			destination: 
 			{ name: 'Prague Vaclav Havel Airport',
 			code: [Object],
 			position: [Object],
 			timezone: [Object],
 			visible: true,
 			info: [Object] },
 			real: null },
 			time: 
 			{ scheduled: { departure: 1521290100, arrival: 1521297000 },
 			real: { departure: null, arrival: null },
 			estimated: { departure: null, arrival: null },
 			other: { eta: null, duration: null } } } { identification: 
 				{ id: '10ba825b',
 				row: 4742968451,
 				number: { default: 'AZ1646', alternative: null },
 				callsign: null,
 				codeshare: [ 'AF9791' ] },
 				status: 
 				{ live: false,
 					text: 'Departed 06:15',
 					icon: 'green',
 					estimated: null,
 					ambiguous: false,
 					generic: { status: [Object], eventTime: [Object] } },
 					aircraft: 
 					{ model: { code: 'A319', text: 'Airbus A319-112' },
 					hex: '4CA8DE',
 					registration: 'EI-IME',
 					serialNo: null,
 					age: { availability: true },
 					availability: { serialNo: true, age: true } },
 					owner: 
 					{ name: 'Alitalia',
 					logo: 's3:AZ_AZA.png',
 					code: { iata: 'AZ', icao: 'AZA' } },
 					airline: 
 					{ name: 'Alitalia',
 					short: 'Alitalia',
 					code: { iata: 'AZ', icao: 'AZA' } },
 					airport: 
 					{ origin: { timezone: [Object], info: [Object] },
 					destination: 
 					{ name: 'Milan Linate Airport',
 					code: [Object],
 					position: [Object],
 					timezone: [Object],
 					visible: true,
 					info: [Object] },
 					real: null },
 					time: 
 					{ scheduled: { departure: 1521176700, arrival: 1521182100 },
 					real: { departure: 1521177324, arrival: 1521181962 },
 					estimated: { departure: null, arrival: null },
 					other: { eta: 1521181962, duration: null } } }
 					*/